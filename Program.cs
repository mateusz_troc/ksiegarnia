﻿using System;
using System.Collections.Generic;

namespace ConsoleAppBiblioteka
{
    class Program
    {
        static void Main(string[] args)
        {
            var ksiazki = new List<Książka>() { };
            var ksiazki_fab = new List<KsiążkaFabularna>() { };
            var magazyny = new List<Magazyn>() { };
            var magazyny_mot = new List<MagazynMotoryzacyjny>() { };
            string answ = "";
            int ks = -1;
            while (answ != "5")
            {
                answ = "";
                string aansw = "";
                string aaansw = "";
                var tansw = new List<string> {"1", "2", "3", "4", "5"};
                while (!tansw.Contains(answ))
                {
                    Console.WriteLine(
                        "Witaj w naszej księgarni, chciałbyś dodać książkę/magazyn do księgarni (wybierz opcję 1)");
                    Console.WriteLine("Wyświetlić wszystkie książki (wybierz opcję 2)");
                    Console.WriteLine("Wyświetlić wszystkie magazyny (wybierz opcję 3)");
                    Console.WriteLine("Wyświetlić wszystko (wybierz opcję 4)");
                    Console.WriteLine("Zakończyć program (wybierz 5)");
                    answ = Console.ReadLine();
                }

                if (answ == "1")
                {
                    while (aansw is not ("1" or "2"))
                    {
                        Console.WriteLine("Chciałbyś dodać książkę (wybierz 1) czy magazyn (wybierz 2)?");
                        aansw = Console.ReadLine();
                    }
                    if (aansw == "1")
                    {
                        
                        while (aaansw is not ("1" or "2"))
                        {
                            Console.WriteLine("Chciałbyś dodać książkę (wybierz 1) czy książkę fabularną (wybierz 2)?");
                            aaansw = Console.ReadLine();
                        }

                        if (aaansw == "1")
                        {
                            Console.WriteLine("Podaj numer wydania:");
                            var n = Console.ReadLine();
                            Console.WriteLine("Podaj tytuł:");
                            var t = Console.ReadLine();
                            Console.WriteLine("Podaj autora:");
                            var a = Console.ReadLine();
                            ksiazki.Add(new Książka() {numer_wydania = n, tytuł = t, autor = a}); 
                        }

                        if (aaansw == "2")
                        {
                            Console.WriteLine("Podaj numer wydania:");
                            var n = Console.ReadLine();
                            Console.WriteLine("Podaj tytuł:");
                            var t = Console.ReadLine();
                            Console.WriteLine("Podaj autora:");
                            var a = Console.ReadLine();
                            ksiazki_fab.Add(new KsiążkaFabularna() {numer_wydania = n, tytuł = t, autor = a}); 
                        }
                    }
                    if (aansw == "2")
                    {
                        while (aaansw is not ("1" or "2"))
                        {
                            Console.WriteLine("Chciałbyś dodać magazyn (wybierz 1) czy magazyn motoryzacyjny (wybierz 2)?");
                            aaansw = Console.ReadLine();
                        }

                        if (aaansw == "1")
                        {
                            Console.WriteLine("Podaj producenta:");
                            var p = Console.ReadLine();
                            Console.WriteLine("Podaj rok wydania:");
                            var r = Console.ReadLine();
                            Console.WriteLine("Podaj tytuł:");
                            var t = Console.ReadLine();
                            magazyny.Add(new Magazyn() {producent = p, rok_wydania = r, tytuł = t}); 
                        }
                        if (aaansw == "2")
                        {
                            Console.WriteLine("Podaj producenta:");
                            var p = Console.ReadLine();
                            Console.WriteLine("Podaj rok wydania:");
                            var r = Console.ReadLine();
                            Console.WriteLine("Podaj tytuł:");
                            var t = Console.ReadLine();
                            magazyny_mot.Add(new MagazynMotoryzacyjny() {producent = p, rok_wydania = r, tytuł = t}); 
                        }
                    }
                }

                if (answ == "2")
                {
                    foreach (var x in ksiazki)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in ksiazki_fab)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }
                
                if (answ == "3")
                {
                    foreach (var x in magazyny)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in magazyny_mot)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }

                if (answ == "4")
                {
                    foreach (var x in ksiazki)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in ksiazki_fab)
                    {
                        Console.WriteLine(x.ToString());
                    }
                    
                    foreach (var x in magazyny)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in magazyny_mot)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }
            }
        }
    }
    class Książka
    {
        internal string numer_wydania;
        internal string tytuł;
        internal string autor;

        public void ustaw_nr(string nr)
        {
            numer_wydania = nr;
        }

        public void ustaw_tytuł(string ty)
        {
            tytuł = ty;
        }

        public void ustaw_autor(string at)
        {
            autor = at;
        }
        public override string ToString()
        {
            return "Numer Wydania: " + numer_wydania.ToString() + " Tytuł: " + tytuł.ToString() + " Autor: " + autor.ToString();
        }
    }
    class KsiążkaFabularna : Książka
    {
        private string rodzaj = "Książka Fabularna";
    }
    class Magazyn
    {
        internal string producent;
        internal string rok_wydania;
        internal string tytuł;
        public void ustaw_producent(string at)
        {
            producent = at;
        }
        public void ustaw_rok(string nr)
        {
            rok_wydania = nr;
        }
        public void ustaw_tytuł(string ty)
        {
            tytuł = ty;
        }
        public override string ToString()
        {
            return "Producent: " + producent.ToString() + " Rok Wydania: " + rok_wydania.ToString() + " Tytuł: " + tytuł.ToString();
        }
    }
    class MagazynMotoryzacyjny : Magazyn
    {
        private string rodzaj = "Magazyn Motoryzacyjny";
    }
}